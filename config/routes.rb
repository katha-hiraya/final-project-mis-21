Rails.application.routes.draw do
  
  
  devise_for :admins 
  devise_scope :cashier do
    authenticated :cashier do
      root 'orders#main' , as: :authenticated_roots
      
      resources :orders do
                collection do
                  post :summary, to: 'orders#order_save'
                end
              end
              
          get 'orderlines/index'
              resources :orderlines
              
        unauthenticated do 
	        root 'devise/sessions#landing_page', as: :unauthenticated_roots
        end

    end
  end
 
 
    devise_scope :admin do
      authenticated :admin do
        root 'admins#index', as: :authenticated_root
    
        get 'admin/index'
          resources :admins
          
        #get 'cashiers/transactions'
          resources :orders
          
        get 'cashiers/daily_report'
          resources :orders
          
        get 'products/create_products' #if not working ~> change to admings
          resources :products
           
        get 'cashiers/create'
          resources :cashiers
        
        end
      end
    unauthenticated do 
	    root 'devise/sessions#landing_page', as: :unauthenticated_root
    end

    
    devise_for :cashier
      # devise_scope :cashier do
      #   authenticated :cashier do
      #     root 'orders#main' , as: :authenticated_roots 
        
      #         resources :orders do
      #           collection do
      #             post :summary, to: 'orders#order_save'
      #           end
      #         end
              
      #     get 'orderlines/index'
      #         resources :orderlines
              
      #   unauthenticated do 
	     #   root 'devise/sessions#landing_page', as: :unauthenticated_roots
      #   end

              
      #   end
      # end
end