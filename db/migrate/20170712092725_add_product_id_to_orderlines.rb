class AddProductIdToOrderlines < ActiveRecord::Migration[5.1]
  def change
    add_reference :orderlines, :product, foreign_key: true
  end
end
