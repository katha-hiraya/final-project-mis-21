class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product_name
      t.decimal :product_price
      t.boolean :product_status

      t.timestamps
    end
  end
end
