class CreateOrderlines < ActiveRecord::Migration[5.1]
  def change
    create_table :orderlines do |t|
      t.integer :order_line_quantity
      t.decimal :order_line_price

      t.timestamps
    end
  end
end
