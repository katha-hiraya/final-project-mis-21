class Order < ApplicationRecord
  belongs_to :cashier
  has_many :products
  has_many :orderlines
  accepts_nested_attributes_for :cashier, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :orderlines, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :products, reject_if: :all_blank, allow_destroy: true
  
  def to_s
    product_name
  end
end
