class CashiersController < ApplicationController
  before_action :set_cashier, only: [:show, :edit, :update, :destroy, :transactions]
  
  def index
    @cashiers = Cashier.all
  end
  
  def new
    @cashier = Cashier.new
  end
  
  def create
    @cashier = Cashier.new(cashier_name: cashier_params["cashier_name"], cashier_status: true, admin_id: current_admin.id, password: "password", email: cashier_params["email"]) #check
    # raise @cashier.inspect
    if @cashier.save
      redirect_to @cashier
    end
  end
  
  def show
  end
  
  def edit
  end
  
  def daily_report
    @orders = Order.all
  end
  
  def update
    if @cashier.update(cashier_params)
      redirect_to @cashier
    end
  end
  
  def destroy
   if @cashier.cashier_status == true
      @cashier= Cashier.update(params[:id], cashier_status: false) 
      redirect_to cashiers_path
    else
      @cashier= Cashier.update(params[:id], cashier_status: true) 
      redirect_to cashiers_path
    end
  end   
  

  
  private 
    def set_cashier
      @cashier = Cashier.find(params[:id])
    end
    def cashier_params
      params.require(:cashier).permit!
    end
  
end
