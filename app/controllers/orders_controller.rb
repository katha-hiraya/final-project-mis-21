class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  
  
  def index
    @orders = Order.all
    @orderlines = Orderline.all
  end
  
  def new
    @order = Order.new
    # @orderline = Orderline.new
    # @orderlines = Orderline.new
  end
  
  
  def create
    @order = current_cashier.orders.new(order_params)
    # @orderline = Orderline.new(params.permit(:order_id, :product_id, :order_line_quantity, :order_line_price))
    # @orderlines = Orderline.new(orderline_params)
    # @orderline = Orderline.new(order_id: @order.id, product_id: orderline.product_id, order_line_quantity: orderline.order_line_quantity, order_line_price: orderline.order_line_price)
   
  end
  
  def order_save
    @order = Order.new(order_params)
    # raise @order.inspect
    #@order.orderlines << Orderline.where('order_line_quantity' => params[:order_params]).first
    #@order.save
    if @order.save
      redirect_to @order
    end
  end
  
  def show
  end
  
  def to_s
  end
  
  private
    def set_order
      @order = Order.find_by(id: params[:id])
      #redirect_to orders_path if !@order.present?
    end
    
    def order_params
      params.require(:order).permit!
    end
    
    #def set_orderline
    #  @orderline = Orderline.find_by(id: params[:id])
    #end
    
    #def orderline_params
    #  params.require(:orderline).permit!
    #end
    
    #def set_orderline
     
     # @orderlines = Orderline.find_by(params[:product_id, :order_id, :order_line_quantity, :product_price])
    #end
    
  
    
end
