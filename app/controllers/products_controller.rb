class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  
  def index
    @products = Product.all
  end
  
  def new
    @product = Product.new
  end
  
  def create
    #product = Product.find(params[:product])
    @product = Product.new(product_name: product_params["product_name"], product_price: product_params["product_price"], product_status:  true)
    if @product.save
        redirect_to @product
    else
      render :new
    end
  end

  def show
    
  end
  
  def to_s
  end
  
  def update
    if @product.update(product_params)
      redirect_to @product
    end
  end
  
  def edit
  end
  
  def destroy
    if @product.product_status == true
      @product = Product.update(params[:id], product_status: false)
      redirect_to products_path
    else
      @product = Product.update(params[:id], product_status: true)
      redirect_to products_path
    end
  end
  
  private
    def set_product
      @product = Product.find(params[:id])
    end
    def product_params
      params.require(:product).permit!
    end
end
